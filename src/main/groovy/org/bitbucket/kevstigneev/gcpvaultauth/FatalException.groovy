package org.bitbucket.kevstigneev.gcpvaultauth

import groovy.transform.InheritConstructors

/**
 * Indicates a fatal error case.
 */
@InheritConstructors
class FatalException extends Exception {}
