package org.bitbucket.kevstigneev.gcpvaultauth

import com.bettercloud.vault.Vault
import com.bettercloud.vault.VaultConfig
import com.bettercloud.vault.VaultException
import com.bettercloud.vault.response.AuthResponse
import groovy.util.logging.Slf4j
import org.quartz.DateBuilder
import org.quartz.Job
import org.quartz.JobBuilder
import org.quartz.JobDataMap
import org.quartz.JobExecutionContext
import org.quartz.JobExecutionException
import org.quartz.Scheduler
import org.quartz.SimpleScheduleBuilder
import org.quartz.TriggerBuilder
import org.quartz.impl.StdSchedulerFactory
import org.quartz.listeners.JobListenerSupport

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST
import static java.net.HttpURLConnection.HTTP_FORBIDDEN
import static org.quartz.DateBuilder.IntervalUnit.*
import static org.quartz.impl.matchers.KeyMatcher.*
import static org.quartz.JobKey.*

/**
 * Maintains connection to Hashicorp Vault.
 *
 * In particular it renews Vault authentication token. It tries to manage
 * token lease time on its own, so the lease time for the token will likely be
 * different soon after VaultKeeper.start(). But may be ignored by Vault.
 */
@Slf4j
class VaultKeeper {

    int vaultMaxRetries = 3
    int vaultRetryIntervalSeconds = 5

    /**
     * Vault client configuration. Requires environment variables:
     * <ul>
     *     <li>VAULT_ADDR   - URL to the Vault server, e.g. http://localhost:8200.
     *     <li>VAULT_TOKEN  - a valid Vault authentication token.
     * </ul>
     */
    VaultConfig vaultConfig = new VaultConfig().build()

    /**
     * Vault "driver". Practically it's {@link VaultConfig} with few more static fields.
     */
    Vault vaultDriver = new Vault(vaultConfig)
            .withRetries(vaultMaxRetries, vaultRetryIntervalSeconds * 1000)

    /**
     * Quartz scheduler running token renewal jobs.
     */
    Scheduler scheduler = new StdSchedulerFactory().scheduler

    // Status of VaultKeeper success in renewing Vault token.
    private volatile VaultStatus status = VaultStatus.NA

    private String myName = this.class.name.tokenize('.').last()

    private static volatile VaultKeeper singleInstance

    private VaultKeeper() {}

    /**
     * The class has to be a singleton.
     */
    synchronized static VaultKeeper getInstance() {
        singleInstance ?: (singleInstance = new VaultKeeper())
    }

    /**
     * VaultKeeper health indicator.
     * @return <code>TRUE</code> if the Vault token has bees successfully renewed.
     */
    Boolean getHealthy() {
        status in [VaultStatus.RENEWED]
    }

    /**
     * VaultKeeper readiness indicator.
     * @return <code>TRUE</code> if the Vault server can technically be accessed.
     */
    Boolean getReady() {
        status in [VaultStatus.ACCESSED, VaultStatus.RENEWED]
    }

    /**
     * The managed Vault connection. Also it checks if VaultKeeper got a final
     * failure accessing Vault and throws {@link FatalException} in that case.
     * @return Vault driver instance if it's ready, <code>null</code> otherwise.
     */
    Vault getVault() {
        if (status == VaultStatus.FORBIDDEN) {
            log.error "Vault access is forbidden"
            throw new FatalException("Vault access is forbidden")
        }
        ready ? vaultDriver : null
    }

    /**
     * Start a background job renewing the Vault token ({@link VaultTokenRenewJob}).
     */
    synchronized void start() {
        assert !scheduler.isShutdown(), "Quartz scheduler cannot be restarted after shutdown"
        if (scheduler.isStarted()) return

        log.info "Schedule the background job to renew token for Vault server at ${vaultConfig.address}"

        // Schedule the job to run once.
        // Then it will figure out the token lease time and configure itself for recurring runs.
        def jobData = [
                'vault': vaultDriver,
        ]
        def jobId = jobKey("VaultTokenRenewJob", myName)
        def job = JobBuilder.newJob(VaultTokenRenewJob)
                .withIdentity(jobId)
                .usingJobData(new JobDataMap(jobData))
                .build()
        def trigger = TriggerBuilder.newTrigger()
                .withIdentity("${jobId.name}-once", jobId.group)
                .startNow()
                .build()

        // Listen to job runs, get the job result and update VaultKeeper health status
        def jobListener = new JobListenerSupport() {
            String name = jobId as String
            void jobWasExecuted(JobExecutionContext context, JobExecutionException _) {
                log.debug "${name} listener got job execution result ${context.result}"
                status = context.result?.status ?: VaultStatus.NA
                log.info "Set VaultKeeper status to ${status}"
            }
        }

        scheduler.listenerManager.addJobListener(jobListener, keyEquals(jobId))
        scheduler.scheduleJob(job, trigger)
        scheduler.start()
    }

    synchronized void stop() {
        scheduler.shutdown(true)
        log.info "Vault token renewal job stopped"
    }
}

/**
 * Quartz job renewing Vault access token.
 */
@Slf4j
class VaultTokenRenewJob implements Job {

    private String myName = this.class.name.tokenize('.').last()

    void execute(JobExecutionContext context) {
        log.debug "Started job to renew Vault authentication token with JobDataMap ${context.mergedJobDataMap}"

        context.result = [:]
        context.result.status = VaultStatus.NA

        // Get job parameters passed at scheduling time
        def vault = context.mergedJobDataMap.vault as Vault
        assert vault

        log.info "Renewing Vault authentication token"
        def (status, response) = renewToken(vault)
        context.result.status = status
        if (status != VaultStatus.RENEWED) return

        def nextFireTime = context.trigger.nextFireTime

        if (!context.trigger.mayFireAgain()) {
            // If we're here it is a 1st run of the job.
            // Schedule it to run periodically at 1/2 of token expiration time.
            def tokenRenewSeconds = response.authLeaseDuration / 2 as Integer

            log.debug "Vault token lease duration is ${response.authLeaseDuration} seconds"
            log.info "Schedule recurring renewal job runs each ${tokenRenewSeconds} seconds"
            def scheduleBuilder = SimpleScheduleBuilder.simpleSchedule()
                    .withIntervalInSeconds(tokenRenewSeconds)
                    .repeatForever()
            def trigger = TriggerBuilder.newTrigger()
                    .withIdentity(myName, context.trigger.key.group)
                    .withSchedule(scheduleBuilder)
                    .startAt(DateBuilder.futureDate(tokenRenewSeconds, SECOND))
                    .build()
            nextFireTime = context.scheduler.rescheduleJob(context.trigger.key, trigger)
        }

        log.info "Vault token renewal completed, next one is scheduled at ${nextFireTime}"
    }

    static Tuple2<VaultStatus, AuthResponse> renewToken(Vault vault) {
        try {
            new Tuple2(VaultStatus.RENEWED, vault.auth().renewSelf())
        } catch (VaultException e) {
            log.error "Vault token renewal failed: ${e}"
            log.warn "If it's the 1st run of the Vault token renewal job, it won't run next time!"
            switch (e.httpStatusCode) {
                case HTTP_BAD_REQUEST:
                    // The token is not renewable. Will become HTTP_FORBIDDEN soon.
                    new Tuple2(VaultStatus.ACCESSED, null)
                    break
                case HTTP_FORBIDDEN:
                    // The token is not valid or expired. Fatal case.
                    new Tuple2(VaultStatus.FORBIDDEN, null)
                    break
                default:
                    // Connection refused/timeout ("0", no Vault at the other end). Or something else.
                    new Tuple2(VaultStatus.NA, null)
                    break
            }
        }
    }
}

/**
 * Status codes for Vault token renew job:
 * <ul>
 *     <li>NA        - Failed to reach Vault. Worth to retry later.
 *     <li>FORBIDDEN - Vault forbids access. A terminal failure, bail out.
 *     <li>ACCESSED  - Interim success, e.g. Vault is accessible but token renewal failed. May soon turn to failure.
 *     <li>RENEWED   - Token renewed, full success.
 * </ul>
 */
enum VaultStatus {
    NA, FORBIDDEN, ACCESSED, RENEWED
}