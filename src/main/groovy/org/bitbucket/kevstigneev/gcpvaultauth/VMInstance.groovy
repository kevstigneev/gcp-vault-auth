package org.bitbucket.kevstigneev.gcpvaultauth

import com.google.cloud.compute.Compute
import com.google.cloud.compute.ComputeOptions
import com.google.cloud.compute.Instance
import com.google.cloud.compute.InstanceId
import com.google.cloud.compute.Zone
import com.google.cloud.compute.ZoneId
import groovy.util.logging.Slf4j

/**
 * Interactions with GCE Instance API.
 */
@Slf4j
class VMInstance {

    Compute computeService
    InstanceId instanceId
    Instance vmInstance

    VMInstance(String zone, String name) {
        assert zone && name
        instanceId = InstanceId.of(zone, name)
    }

    VMInstance(String project, String zone, String name) {
        assert project && zone && name
        instanceId = InstanceId.of(project, zone, name)
    }

    synchronized Compute getCompute() {
        try {
            computeService ?:
                    (computeService = ComputeOptions.defaultInstance.service)
        } catch (IllegalArgumentException e) {
            log.error "Cannot find GCP project: ${e}"
            log.error "Either GCP API is not configured locally" +
                    " or GCE instance doesn't have access to the instance metadata."
            throw new FatalException("Cannot determine the local GCP project", e)
        }
    }

    Instance setInstance(InstanceId id) {
        vmInstance = (!id.project ||(id.project == compute.options.projectId)) ?
                compute.getInstance(id) :
                null
    }

    synchronized Instance getInstance() {
        vmInstance ?: setInstance(instanceId)
    }

    /**
     * Queries GCP API for service accounts of the VM name.
     * @return  List of service accounts emails.
     */
    List<String> getServiceAccounts() {
        instance?.serviceAccounts*.email ?: []
    }

    /**
     * Queries GCP API for internal IP addresses of the VM instance.
     * @return  List of internal IP addresses.
     */
    List<String> getNetworkAddresses() {
        instance?.networkInterfaces*.networkIp ?: []
    }

    /**
     * Sanity check aiming to validate that GCE API is accessible enough to
     * fulfil the application duties.
     * It tries to lookup a not existing but correctly identified GCE instance.
     * An exception is thrown for any failures.
     * @return <code>TRUE</code>
     */
    static Boolean sanityCheck() {
        log.debug "Obtain GCE Compute service"
        def compute = ComputeOptions.defaultInstance.service
        log.debug "Get a legitimate GCE availability zone"
        def zones = compute.listZones(
                Compute.ZoneListOption.filter(Compute.ZoneFilter.equals(Compute.ZoneField.STATUS, Zone.Status.UP as String)),
                Compute.ZoneListOption.pageSize(1)
        )
        ZoneId zone = zones.values.first().get().zoneId
        def name = "not-existing-instance"
        log.debug "Lookup for a (not existing) GCE VM instance '${name}' in zone '${zone}'"
        compute.getInstance(InstanceId.of(zone, name))
        true
    }

    @Override
    String toString() {
        (vmInstance ?: instanceId) as String
    }
}