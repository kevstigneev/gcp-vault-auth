package org.bitbucket.kevstigneev.gcpvaultauth

import com.bettercloud.vault.Vault
import com.bettercloud.vault.VaultConfig
import com.bettercloud.vault.VaultException
import groovy.util.logging.Slf4j

import static java.net.HttpURLConnection.HTTP_FORBIDDEN

/**
 * Interactions with Hashicorp Vault.
 */
@Slf4j
class VaultClient {

    Vault vault = new Vault(new VaultConfig().build())

    /**
     * Where role <-> service_account mapping is in Vault.
     */
    final String aclKey = "secret/approle-acl"

    /**
     * Use a known secrets folder ("${aclKey}/") to check if the
     * service account is allowed to get Vault credentials for the role.
     * The folder is populated by an operator with keys like
     * "${aclKey}/<role_name>/<account_email>".
     * @param role      AppRole name.
     * @param account   Service account email.
     * @return          True if the service account is permitted.
     */
    Boolean rolePermitsAccount(String role, String account) {
        log.debug "Checking if account '${account}' is listed for '${role}' in Vault"

        try {
            log.trace "Check if '${aclKey}/${role}/' exists"
            def roles = vault.logical().list("${aclKey}/")*.minus('/')
            if (!(role in roles)) return false

            log.trace "Check if '${aclKey}/${role}/${account}/' exists"
            def accounts = vault.logical().list("${aclKey}/${role}/")*.minus('/')
            account in accounts
        } catch (VaultException e) {
            log.error "Failed to list Vault keys under ${aclKey}: ${e}"
            handleVaultError(e)
            false
        }
    }

    /**
     * Get SecretID for AppRole. Limit SecretID use to the specific instance.
     * @param role      AppRole name.
     * @param address   IP address of the instance.
     * @return          AppRole SecretID.
     */
    String getApproleSecretID(String role, String address) {
        log.debug "Requesting AppRole SecretID for role '${role}'"

        try {
            def secretProps = [
                    cidr_list: address + "/32"]
            def response = vault.logical()
                    .write("auth/approle/role/${role}/secret-id", secretProps)
            response.data.secret_id
        } catch (VaultException e) {
            log.error "Failed to get SecretID for AppRole '${role}': ${e}"
            handleVaultError(e)
            ""
        }
    }

    /**
     * Sanity check for Vault connection. Throw an exception if something is wrong.
     * @return <code>TRUE</code>.
     */
    Boolean check() {
        log.debug "Vault sanity check - check if '${aclKey}/' keys are readable"
        try {
            vault.logical().list("${aclKey}/")
        } catch (VaultException e) {
            log.warn "Vault sanity check failed: ${e}"
            handleVaultError(e)
            throw e
        }
        true
    }

    /**
     * Decide what actions should be taken in a case of a Vault error.
     * E.g. if the error is fatal and the application should terminate or not.
     * @param e         Exception thrown by a Vault API call.
     */
    static void handleVaultError(VaultException e) {
        if (e.httpStatusCode == HTTP_FORBIDDEN) {
            log.error "Access denied to Vault - probably Vault ACL role is not configured properly"
            throw new FatalException("Access denied to Vault", e)
        }
    }

    /**
     * A rudimentary sanity check of Vault API configuration. It just tests if
     * Vault responds and is unsealed. It doesn't check if the provided Vault
     * credentials are valid. It throws an exception at any error.
     * @return <code>TRUE</code>.
     */
    static Boolean sanityCheck() {
        log.debug "Validate Vault configuration"
        def config = new VaultConfig().build()
        assert config.token, "Vault token is not set"
        def vault = new Vault(config)
        log.debug "Get Vault health status, check if it's unsealed"
        !vault.debug().health().sealed
    }
}