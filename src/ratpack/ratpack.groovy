/**
 * Ratpack server.
 * Launch directly or by ratpack.groovy.GroovyRatpackMain.main().
 */

import com.fasterxml.jackson.annotation.*
import groovy.util.logging.Slf4j
import org.bitbucket.kevstigneev.gcpvaultauth.*
import org.slf4j.LoggerFactory
import ratpack.exec.Blocking
import ratpack.exec.Promise
import ratpack.func.Pair
import ratpack.handling.RequestLogger
import ratpack.health.HealthCheck
import ratpack.health.HealthCheckHandler
import ratpack.registry.Registry
import ratpack.server.ServerConfig
import ratpack.server.Stopper
import ratpack.service.DependsOn
import ratpack.service.Service
import ratpack.service.StartEvent
import ratpack.service.StopEvent

import static ratpack.groovy.Groovy.ratpack
import static ratpack.jackson.Jackson.fromJson
import static java.net.HttpURLConnection.HTTP_OK
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST
import static java.net.HttpURLConnection.HTTP_FORBIDDEN
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR

def log = LoggerFactory.getLogger(this.class)

ratpack {

    bindings {
        add VaultKeeper.instance

        add new StartupCheckService()
        add new ShutdownService()
        add new VaultKeeperService()

        add HealthCheck.of("vault-keeper") { registry ->
            def vaultKeeper = registry.get(VaultKeeper)
            Promise.sync {
                (vaultKeeper.healthy && vaultKeeper.ready) ?
                        HealthCheck.Result.healthy() :
                        HealthCheck.Result.unhealthy("Cannot renew Vault token")
            }
        }

        add HealthCheck.of("vault") { registry ->
            def vaultKeeper = registry.get(VaultKeeper)
            Blocking.get {
                assert vaultKeeper.vault, "Vault is not available"
                def vault = new VaultClient(vault: vaultKeeper.vault.withRetries(2, 100))
                vault.check()
            }.map {
                HealthCheck.Result.healthy()
            }.mapError(FatalException) { error ->
                log.error "Vault health check fatally failed - ${error.message}. Terminating the application"
                bailout registry, 1
                HealthCheck.Result.unhealthy("Fatal error")
            }.mapError { error ->
                log.warn "Vault health check failed: ${error}"
                HealthCheck.Result.unhealthy("Cannot reach Vault")
            }
        }
    }

    handlers {
        get("status/health", new HealthCheckHandler())
        get("status/health/:name", new HealthCheckHandler())

        all(RequestLogger.ncsa())

        prefix("auth/approle/role") {
            post (":role/secret-id") {
                def appRole = pathTokens.role
                assert appRole

                def remoteHost = request.remoteAddress?.hostText
                assert remoteHost, "Cannot identify the requesting host"

                log.debug "${request.method} request from ${remoteHost} to ${request.path}"

                def vaultKeeper = get(VaultKeeper)

                parse(fromJson(SecretIdRequest)).onError { e ->
                    log.error "Invalid input: ${e}"
                    response.status HTTP_BAD_REQUEST
                    render "Invalid input: ${e.message}"
                }.flatMap { requestData ->
                    def vm = requestData.project ?
                            new VMInstance(requestData.project, requestData.zone, requestData.instance) :
                            new VMInstance(requestData.zone, requestData.instance)
                    def vault = new VaultClient(vault: vaultKeeper.vault)
                    assert vaultKeeper.ready, "VaultKeeper is not ready"

                    Blocking.get {
                        if (!vm.instance) {
                            log.error "Instance ${vm} not found in the current project"
                            Pair.of HTTP_BAD_REQUEST, "VM instance not found"
                        } else if (!(remoteHost in vm.networkAddresses) && !isLocalhost(remoteHost)) {
                            log.error "Forbidden: request source (${remoteHost}) and VM ${vm} addresses do not match"
                            Pair.of HTTP_FORBIDDEN, "The remote host is not allowed"
                        } else if (!vm.serviceAccounts.any { vault.rolePermitsAccount(appRole, it) }) {
                            log.error "Forbidden: role ${appRole} is not permitted for accounts ${vm} service accounts"
                            Pair.of HTTP_FORBIDDEN, "The VM instance service accounts are not allowed"
                        } else {
                            log.info "Authorised ${remoteHost} for AppRole '${appRole}'"
                            def secret = vault.getApproleSecretID(appRole, remoteHost)
                            if (secret) {
                                log.debug "Send SecretID to the requester"
                                Pair.of HTTP_OK, secret
                            } else {
                                log.error "The role '${appRole}' is configured in the ACL but SecretID request failed" +
                                        " - maybe the Approle is not configured"
                                Pair.of HTTP_INTERNAL_ERROR, "Cannot get SecretID"
                            }
                        }
                    }
                }.onError(FatalException) { e ->
                    // Not feasible to continue operation, terminate.
                    // Hopefully on restart the issue gets fixed.
                    log.error "Fatal error state, terminate the server"
                    bailout context, 1

                    response.status HTTP_INTERNAL_ERROR
                    render e.message
                }.then { responsePair ->
                    response.status responsePair.left
                    render responsePair.right
                }
            }
        }
    }
}

static Boolean isLocalhost(String host) {
    host == '127.0.0.1'
}

/**
 * Gracefully terminate the server.
 * The implementation is taken from Taken from https://github.com/ratpack/ratpack/issues/898.
 * @param registry - A server Registry instance, ratpack.handling.Context is fine.
 * @param exitCode - Process exit code, set to non-zero to indicate a failure.
 */
static void bailout (Registry registry, int exitCode) {
    new Thread().start {
        registry.get(Stopper).stop()
        // At this point the server is stopped. Let's set the exit code.
        System.exit(exitCode)
    }
}

/**
 * Deserialized form of SecretID request body.
 * Also in charge of request data validation.
 * Request fields are:
 * <ul>
 *     <li>project  - GCP project ID (optional).
 *     <li>zone     - availability zone of the instance.
 *     <li>instance - VM instance name.
 * </ul>
 */
class SecretIdRequest {
    String project
    String zone
    String instance

    @JsonCreator
    SecretIdRequest(@JsonProperty("project")    String project,
                    @JsonProperty("zone")       String zone,
                    @JsonProperty("instance")   String instance) {
        assert !project || validRFC1035Name(project)
        this.project = project
        assert zone && validRFC1035Name(zone)
        this.zone = zone
        assert instance && validRFC1035Name(instance)
        this.instance = instance
    }

    static boolean validRFC1035Name(String name) {
        name && (name.size() < 64) && (name ==~ /[a-z]([-a-z0-9]*[a-z0-9])?/)
    }

    @Override
    String toString() {
        "{zone: '${zone}', instance: '${instance}'}"
    }
}

@Slf4j
@DependsOn(ShutdownService)
class StartupCheckService implements Service {
    Boolean started = false

    void onStart(StartEvent event) {
        log.info "Do GCE API sanity check"
        assert VMInstance.sanityCheck()
        log.info "GCE sanity check passed"
        log.info "Do Vault sanity check"
        assert VaultClient.sanityCheck()
        log.info "Vault sanity check passed"
        started = true
    }
}

/**
 * Work-around for https://github.com/ratpack/ratpack/issues/1227.
 * For some reason Retpack server JVM does not stop on a Server initialization failure.
 * Terminate it forcefully with non-zero exit code if the server sanity check did not succeed.
 */
@Slf4j
class ShutdownService implements Service {

    void onStop(StopEvent event) {
        def exitCode = event.registry.get(StartupCheckService).started ? 0 : 1
        if (!event.registry.get(ServerConfig).isDevelopment()) {
            // Clean up stale threads and set non-zero exit status
            new Thread().start { System.exit(exitCode) }
        }
    }
}

@Slf4j
@DependsOn(StartupCheckService)
class VaultKeeperService implements Service {

    void onStart(StartEvent event) {
        if (event.isReload()) return            // As of 1.4.5 it doesn't work
        log.info "Starting ${name} service"
        event.registry.get(VaultKeeper).start()
    }

    void onStop(StopEvent event) {
        if (event.isReload()) return            // As of 1.4.5 it doesn't work
        log.info "Stopping ${name} service"
        event.registry.get(VaultKeeper).stop()
    }
}
