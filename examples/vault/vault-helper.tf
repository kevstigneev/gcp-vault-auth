#
# Terraform configuration to setup test data in Vault.
# Setup access for GCP authentication helper.
#
# Use:
#   export VAULT_ADDR=http://127.0.0.1:8200
#   export VAULT_TOKEN=<vault_token>
#   terraform apply

resource "vault_policy" "gcp_vault_auth" {
  name = "gcp-vault-auth"

  policy = <<EOT
path "secret/approle-acl/*" {
    capabilities = ["read", "list"]
}
path "auth/approle/role/*" {
    capabilities = ["update"]
}
EOT
}
