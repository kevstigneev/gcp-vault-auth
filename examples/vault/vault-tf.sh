#!/bin/sh
#
# Apply Terraform configuration
#

vault_container="vault"
vault_address=$(docker inspect $vault_container -f '{{.NetworkSettings.IPAddress}}')

export VAULT_ADDR="http://${vault_address}:8200"
export VAULT_TOKEN="vaultroot"

terraform "$@"