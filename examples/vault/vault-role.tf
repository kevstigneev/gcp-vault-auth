#
# Terraform configuration to setup test data in Vault.
# Create AppRole and role<->account mapping.
#
# Use:
#   export VAULT_ADDR=http://127.0.0.1:8200
#   export VAULT_TOKEN=<vault_token>
#   terraform apply

variable "app" {
  description = "Application name to be used as Vault AppRole name"
}
variable "sa" {
  description = "GCP service account email"
}

resource "vault_generic_secret" "approle_account_mapping" {
  path = "secret/approle-acl/${var.app}/${var.sa}"
  data_json = <<_EOT
{
  "approle": "${var.app}",
  "service_account": "${var.sa}"
}
_EOT
}

resource "vault_generic_secret" "approle" {
  path = "auth/approle/role/${var.app}"
  data_json = <<_EOT
{
  "bind_secret_id": true
}
_EOT
}