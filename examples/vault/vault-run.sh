#!/bin/sh
#
# Run Hashicorp Vault server in development mode.
#

export VAULT_DEV_ROOT_TOKEN_ID="vaultroot"

docker run -d --name=vault \
    -e SKIP_SETCAP=true \
    -e VAULT_DEV_ROOT_TOKEN_ID \
    vault

# The only step you need to take is to set the following environment variable:
#
#    export VAULT_ADDR='http://127.0.0.1:8200'