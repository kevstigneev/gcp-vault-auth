#
# Terraform configuration to setup test data in Vault.
#
# Use:
#   export VAULT_ADDR=http://127.0.0.1:8200
#   export VAULT_TOKEN=<vault_token>
#   terraform apply

provider "vault" {}