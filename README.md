GCP authentication helper for Hashicorp Vault
=============================================

**Deprecated. Superseded by [Vault native GCP authentication feature](https://www.vaultproject.io/docs/auth/gcp.html).**

Design
------

The service receives a request assuming that it comes from a GCE VM instance.
The service checks if the instance is legitimate to act as an Vault
[AppRole](https://www.vaultproject.io/docs/auth/approle.html) and provisions a
SecretID for the AppRole.

Request is HTTP POST to `auth/approle/role/<role_name>/secret-id`.
Request JSON data:

    {
        "project": "<GCP project ID>",
        "zone": "<GCE instance zone>",
        "instance": "<GCE instance name>"
    }

The service:

1. Compares request the source IP address with the internal IP address of the
  instance identified by (project_ID, instance_name, instance_zone).
2. Checks access configuration stored in Vault if a service account associated
  with the VM instance is permitted to act as the AppRole.
3. Gets SecretID from Vault and returns it as the response using
  [Response Wrapping](https://www.vaultproject.io/docs/concepts/response-wrapping.html).

Requirements:

- Response format should be plain text in order to use in shell scripts.
- HTTPS is optional. The returned SecretID is usable only from the originating
  VM instance.

Assumptions:

- The service runs in the same GCP network as the authenticating VMs. Requests
  come from internal addresses without NAT.
- The instance cannot spoof its IP address. It holds if GCP network is not
  controlled by the instance.


API
---

API is HTTP RPC. Request data is JSON map, `Content-Type: application/json`
header is required.

### /auth/approle/role/{role}/secret-id

#### POST

**Description:** get SecretID for the named AppRole `{role}`.

**Parameters:**

| Parameter     | Description                                   |
|---------------|-----------------------------------------------|
| `project`     | (optional) GCP project ID.                    |
| `zone`        | Availability zone of the instance.            |
| `instance`    | VM instance name.                             |

**Returns:** SecretID for the AppRole. Response type is text/plain.

### /status/health

#### GET

**Description:** get binary server health indicator.

**Returns:** HTTP response code `200` indicates that the server is healthy and
ready to serve requests. If the server is not ready yet or failed to operate,
the HTTP response code is `503`. Response type is text/plain.



Run the service
---------------

To run the service (assuming the running user is authenticated at a GCP
project or it starts at a GCE instance with a capable service account):

    export VAULT_ADDR=http://127.0.0.1:8200
    export VAULT_TOKEN=<vault_token>
    java \
        -Dorg.slf4j.simpleLogger.defaultLogLevel=info \
        -Dratpack.port=5050 \
        -jar gcp-vault-auth.jar

- `ratpack.port` - TCP port to accept HTTP requests. Ratpack honors `PORT`
  environment variable if the `ratpack.port` system property is not set.
  Otherwise it listens at port 5050.
- `ratpack.address` - the IP address to listen at. All addresses by default.
- `org.slf4j.simpleLogger.defaultLogLevel` - logging level. Must be one of
  ("trace", "debug", "info", "warn", "error" or "off"). If not specified,
   defaults to "info".

GCP Authentication. **google-cloud-java** library uses the same authentication
facilities as gcloud API do. If it runs at a GCE instance it's authorized as
the instance service account. If it runs at a standalone machine or a
developer's laptop it uses the default GCP credentials of the current user.
See [Authentication](https://github.com/GoogleCloudPlatform/google-cloud-java#authentication)
in google-cloud-java docs.

Vault authentication. **vault-java-driver** library requires convenience
environment variables set (see
[vault-java-driver](https://github.com/BetterCloud/vault-java-driver) docs for
the full list of controlling environment variables):

- `VAULT_ADDR` - URL to the Vault server (not just HOST:PORT), e.g.
  "http://127.0.0.1:8200".
- `VAULT_TOKEN` - a valid Vault authentication token. The library does not
   work with authentication disabled.

The service uses **SLF4J** for logging. It logs to STDERR by default with is
compatible with Docker and SystemD. See
[SimpleLogger](https://www.slf4j.org/api/org/slf4j/impl/SimpleLogger.html)
API docs for the other system properties that control it.

At startup the service performs few sanity checks, if the checks fail, the
service won't start. The checks are:

- GCE API is accessible.
- The Vault server is unsealed.

If the service encounters a state when operation is no longer feasible, it
exits with non-zero exit status. The terminal cases are:

- When Vault server responds with `403 Access Denied`, that most likely
  indicates that the provided Vault token is either bad or expired.
- When the service fails to get GDP API configuration. It means that either
  gcloud is not configured at the local environment, or the GCE VM instance
  running the service is badly misconfigured.

The expectation is that a service manager (such as Systemd) running this
service restarts it and the bootstrapping script gets it fixed (e.g. obtains a
new Vault token).


Use the service
---------------

### ACL in Vault for the service

The helper service uses Vault generic secret backend as a key-value store to
get (AppRole) <-> (Service Account) mapping. To enable VMs running as a
particular GCP service account to authenticate with a specific AppRole add a
key by Vault CLI (or by
[Generic secret backend HTTP API](https://www.vaultproject.io/api/secret/generic/index.html),
or Terraform configuration at
[examples/vault/vault-role.tf](examples/vault/vault-role.tf)):

    vault write secret/approle-acl/<ROLE_NAME>/<SERVICE_ACCOUT_EMAIL> \
        approle=<ROLE_NAME> \
        service_account=<SERVICE_ACCOUT_EMAIL>

Actually the content is ignored fo far, only the key matters.

### Get Vault token

The Vault token given to the service must be a _periodic token_ capable of:

- Reading from `secret/approle-acl/`.
- Writing to `auth/approle/role/`.

Create a corresponding policy by Vault CLI (also see
[policy HTTP API](https://www.vaultproject.io/api/system/policy.html) and
Terraform configuration in
[examples/vault/vault-helper.tf](examples/vault/vault-helper.tf)):

    vault policy-write gcp-vault-auth - <<_EOF
    path "secret/approle-acl/*" {
        capabilities = ["read", "list"]
    }
    path "auth/approle/role/*" {
        capabilities = ["update"]
    }
    _EOF

To create a periodic token by Vault CLI (also see
[token HTTP API](https://www.vaultproject.io/docs/auth/token.html#api)):

    vault token-create -policy=gcp-vault-auth -period=<TTL_IN_HOURS>h

### Client

    curl -X POST -H 'Content-type: application/json' \
        --data '{"zone":"us-central1-b","instance":"MY_INSTANCE_NAME"}' \
        http://127.0.0.1:5050/auth/approle/role/MY_ROLE/secret-id

**NB!** The service has a debugging short-cut. If the request comes from
localhost, source address validation is skipped.


Performance and resource utilization
------------------------------------

Response time of the service is controlled by GCP API response times. Across
the ocean the service response time is around 1 second. With the default
Ratpack configuration the service handles around 100 RPS with mean response
time 0.8 sec and 1.5 sec at 99th percentile. At higher rate requests start get
dropped with "Too many requests" diagnostics. But the actual throughput is
throttled by
[GCe API rate limit](https://cloud.google.com/compute/docs/api-rate-limits)
that is 20 RPS per GCP project by default. Beyond that point GCP responses
with 403 HTTP code. At 20 RPC the mean response time in 0.8 sec, and 1.0 sec
at 99th percentile.

At this request rate the service runs well with 50 MB limit for JVM heap
(Java 8) hitting full GC once per 5 mins and spending ~0.5% of time in GC.
At OS level resident memory size is around 300 MB, CPU use is negligible.


Limitations
-----------

The service sees VMs ony in the current project. While in an XPN network
requests may come from VMs of different projects.
TODO: Needs at least validate that the request came from the same project as
the one of the Vault server.

**vault-java-driver** 2.0 doesn't support Vault response wrapping. So it isn't
implemented.


Server framework
----------------

It is a [Ratpack](https://ratpack.io/) application. It is implemented using
Ratpack DSL, so the entry point is `src/ratpack/ratpack.groovy`. Rewriting it
in pure Java 8 should not be too difficult.


Misc
----

### Useful links

Ratpack:

- [Ratpack](https://ratpack.io/),
  [ratpack/ratpack](https://github.com/ratpack/ratpack) at GitHub.
- [Rapid Groovy Web Application Development with Ratpack](https://danhyun.github.io/2016-gr8confeu-rapid-ratpack-groovy/)
  by Daniel Hyun - 3 June, 2016.
- [Binge streaming Web APIs, with Ratpack, Cloud Endpoints, App Engine Flex and Streamdata.io](http://glaforge.appspot.com/article/binge-streaming-web-apis-with-ratpack-cloud-endpoints-app-engine-flex-and-streamdata-io)
  by Guillaume Laforge - 15 November, 2016.

Google API by Google:

- [Google API Explorer - Compute](https://developers.google.com/apis-explorer/#p/compute/v1/).
- [google-cloud-java](http://googlecloudplatform.github.io/google-cloud-java/),
  GitHub [GoogleCloudPlatform/google-cloud-java](https://github.com/GoogleCloudPlatform/google-cloud-java).
  As of March 2017 is actively developed, in Alpha.
  As said in FAQ:
  > The [Google APIs Java Client](https://github.com/google/google-api-java-client) is a client library for using the broad set of Google APIs. `google-cloud` is built specifically for the Google Cloud Platform and is the recommended way to integrate Google Cloud APIs into your Java applications.
- [Google API Client Libraries > Java](https://developers.google.com/api-client-library/java/),
  GitHub [google/google-api-java-client](https://github.com/google/google-api-java-client).
  Looks like an abandoned project, last update on March 2016.

Hashicorp Vault:

- [Hashicorp Vault](https://www.vaultproject.io/).
- [BetterCloud/vault-java-driver](https://github.com/BetterCloud/vault-java-driver).

Other libs and tools:

- [Quartz Scheduler](http://www.quartz-scheduler.org/).
- [SLF4J](https://www.slf4j.org/) logging framework.